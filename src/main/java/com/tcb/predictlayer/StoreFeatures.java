package com.tcb.predictlayer;

import java.sql.SQLException;

import com.java.dao.MysqlConnection;
import com.java.dao.SQLRequests;

public class StoreFeatures {

	private static final String TABLE_FEATURES = "myfeatures1";
	private static final String TABLE_FLOW_1 = "montest34";
	private static final String TABLE_FLOW_2 = "montest28";

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		boolean createtable = SQLRequests.createTablefeatures(MysqlConnection.getConnection(), TABLE_FEATURES);
		if (createtable)
			System.out.println("table Created");
		else
			System.out.println("ubable to create table");
		boolean inserttable = SQLRequests.insertIntoTableFeatures(MysqlConnection.getConnection(), TABLE_FEATURES,
				TABLE_FLOW_1, TABLE_FLOW_2);
		if (inserttable)
			System.out.println("data inserted");
		else
			System.out.println(" ooops data not inserted");

	}

}
