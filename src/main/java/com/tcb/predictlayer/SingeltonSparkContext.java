package com.tcb.predictlayer;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;

public class SingeltonSparkContext {
	
	private static SparkContext sc = null ;
	
	private SingeltonSparkContext(){
	}
     public static SparkContext getSparkConetxt(){
    	 if (sc == null){
    		 sc = new SparkContext(new SparkConf().setAppName("Spark Example").setMaster("local[*]")); 
    	  }
    	  return sc ;
     }
}
