package com.tcb.predictlayer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.mllib.regression.LinearRegressionWithSGD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import scala.Tuple2;

public class CreateFeatures {
	private static final String TABLE_FEATURES = "myfeatures1";

	public static void main(String[] args) {

		Map<String, String> options = new HashMap<String, String>();
		options.put("driver", "com.mysql.jdbc.Driver");
		options.put("url", "jdbc:mysql://localhost:3306/weather" + "?user=" + "root" + "&password=" + "Azerty1");
		options.put("dbtable", TABLE_FEATURES);
		SparkContext sc = new SparkContext(new SparkConf().setAppName("Spark Example").setMaster("local[*]"));
		JavaSparkContext jssc = new JavaSparkContext(sc);
		SQLContext con = new SQLContext(sc);
		DataFrame data = con.read().format("jdbc").options(options).load();
		DataFrame dd = data.select("Reference_eTo", "precipitation", "solar_radiation", "vapor_pressure",
				"air_temperature", "relative_humidity", "dew_point", "wind_speed", "wind_direction", "pdsii");
		List<Row> list = dd.collectAsList();
		JavaRDD<Row> rdd = jssc.parallelize(list);
		JavaRDD<LabeledPoint> rdd2 = rdd.map(new Function<Row, LabeledPoint>() {
			private static final long serialVersionUID = 1L;

			@Override
			public LabeledPoint call(Row arg0) throws Exception {
				double[] v = new double[11];
				double label = 0;
				int i = 0;
				for (i = 0; i < 10; i++) {
					v[i] = arg0.getDouble(i);
				}
				label = arg0.getDouble(9);
				LabeledPoint ll = new LabeledPoint(label, Vectors.dense(v));
				return ll;
			}
		});
		rdd2.cache();
		int numIterations = 100;
		double stepSize = 0.00000001;
		final LinearRegressionModel model = LinearRegressionWithSGD.train(JavaRDD.toRDD(rdd2), numIterations, stepSize);
		JavaRDD<Tuple2<Double, Double>> valuesAndPreds = rdd2.map(new Function<LabeledPoint, Tuple2<Double, Double>>() {
			private static final long serialVersionUID = 1L;

			public Tuple2<Double, Double> call(LabeledPoint point) {
				double prediction = model.predict(point.features());
				return new Tuple2<Double, Double>(prediction, point.label());
			}
		});
		valuesAndPreds.foreach(new VoidFunction<Tuple2<Double, Double>>() {
			private static final long serialVersionUID = 1L;

			@Override
			public void call(Tuple2<Double, Double> arg0) throws Exception {
				System.out.println(arg0);
			}
		});
		double MSE = new JavaDoubleRDD(valuesAndPreds.map(new Function<Tuple2<Double, Double>, Object>() {
			private static final long serialVersionUID = 1L;

			public Object call(Tuple2<Double, Double> pair) {
				return Math.pow(pair._1() - pair._2(), 2.0);
			}
		}).rdd()).mean();
		System.out.println("training Mean Squared Error = " + MSE);
		model.save(jssc.sc(), "javaLinearRegressionWithSGDModel");
		LinearRegressionModel sameModel = LinearRegressionModel.load(jssc.sc(), "javaLinearRegressionWithSGDModel");
		System.out.println(sameModel.toString());
		jssc.close();
	}
}
