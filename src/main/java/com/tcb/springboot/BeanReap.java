package com.tcb.springboot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BeanReap extends JpaRepository<Bean, Long> {
}
