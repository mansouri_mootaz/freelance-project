package com.tcb.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {
	
	@Autowired(required = false)
	public BeanReap beanrep;
	@Autowired(required = false)
	public RepPredict beanrep2;


	@RequestMapping("/prediction")
	String predictedVal(Model model) {
		model.addAttribute("pdsii", beanrep2.findAll());
		return "predictedData";
	}
	
	@RequestMapping("/historic")
	String historicData(Model model) {
		model.addAttribute("pdsii", beanrep.findAll());
		return "HistoricData";
	}

	
//	@RequestMapping("/prediction")
//
//	String realTimePrediction(Model model) {
//		model.addAttribute("pdsii", beanrep2.findAll());
//		return "prediction";
//	}

//	@RequestMapping("/prediction2")
//	String realTimePrediction2(Model model) {
//		int i = 0;
//		for (Predictbean bean : beanrep2.findAll()) {
//			i++;
//			model.addAttribute("valueNumber" + i, bean.getPdsii());
//		}
//		return "prediction2";
//	}

//	@RequestMapping("/greeting")
//	public String greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name,
//			Model model) {
//		model.addAttribute("name", name);
//		return "greeting";
//	}

//	@RequestMapping(value = "/essai")
//	public String sayHelloa() {
//		return "essai"; // the name of the ftl file
//	}
	
//	@RequestMapping("/prediction3")
//	String realTimePrediction3(Model model) {
//		model.addAttribute("pdsii", beanrep2.findOne(new Integer((int) beanrep2.count() - 1)).getPdsii());
//		return "prediction";
//	}
}
