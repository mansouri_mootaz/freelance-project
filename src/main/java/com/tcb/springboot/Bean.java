package com.tcb.springboot;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "myfeatures1")
public class Bean implements Serializable {
	private static final long serialVersionUID = 1L;

	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Integer id;

		public Bean(Integer id, String julian_Date, String reference_eTo, String precipitation, String solar_radiation,
				String vapor_pressure, String air_temperature, String relative_humidity, String dew_point,
				String wind_speed, String wind_direction, String soil_temperature, String pdsii,int hours) {
			super();
			this.hours=hours;
			this.id = id;
			this.Julian_Date = julian_Date;
			this.Reference_eTo = reference_eTo;
			this.precipitation = precipitation;
			this.solar_radiation = solar_radiation;
			this.vapor_pressure = vapor_pressure;
			this.air_temperature = air_temperature;
			this.relative_humidity = relative_humidity;
			this.dew_point = dew_point;
			this.wind_speed = wind_speed;
			this.wind_direction = wind_direction;
			this.soil_temperature = soil_temperature;
			this.pdsii = pdsii;
		}
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getJulian_Date() {
			return Julian_Date;
		}
		public void setJulian_Date(String julian_Date) {
			Julian_Date = julian_Date;
		}
		public String getReference_eTo() {
			return Reference_eTo;
		}
		public void setReference_eTo(String reference_eTo) {
			Reference_eTo = reference_eTo;
		}
		public String getPrecipitation() {
			return precipitation;
		}
		public void setPrecipitation(String precipitation) {
			this.precipitation = precipitation;
		}
		public String getSolar_radiation() {
			return solar_radiation;
		}
		public void setSolar_radiation(String solar_radiation) {
			this.solar_radiation = solar_radiation;
		}
		public String getVapor_pressure() {
			return vapor_pressure;
		}
		public void setVapor_pressure(String vapor_pressure) {
			this.vapor_pressure = vapor_pressure;
		}
		public String getAir_temperature() {
			return air_temperature;
		}
		public void setAir_temperature(String air_temperature) {
			this.air_temperature = air_temperature;
		}
		public String getRelative_humidity() {
			return relative_humidity;
		}
		public void setRelative_humidity(String relative_humidity) {
			this.relative_humidity = relative_humidity;
		}
		public String getDew_point() {
			return dew_point;
		}
		public void setDew_point(String dew_point) {
			this.dew_point = dew_point;
		}
		public String getWind_speed() {
			return wind_speed;
		}
		public void setWind_speed(String wind_speed) {
			this.wind_speed = wind_speed;
		}
		public String getWind_direction() {
			return wind_direction;
		}
		public void setWind_direction(String wind_direction) {
			this.wind_direction = wind_direction;
		}
		public String getSoil_temperature() {
			return soil_temperature;
		}
		public void setSoil_temperature(String soil_temperature) {
			this.soil_temperature = soil_temperature;
		}
		public String getPdsii() {
			return pdsii;
		}
		public void setPdsii(String pdsii) {
			this.pdsii = pdsii;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		public Bean() {
			super();
		}
		
		
		private String Julian_Date; 
	    private String Reference_eTo;
	    private String precipitation;
	    private String solar_radiation;
	    private String vapor_pressure ;
	    private String  air_temperature ;
	    private String relative_humidity ;
	    private String dew_point ;
	    private String wind_speed ;
	    private String wind_direction;
	    private String soil_temperature;
	    private String pdsii ;
		private int hours;

		public int getHours() {
			return hours/100;
		}
		public void setHours(int hours) {
			this.hours = hours;
		}


	    
}
