package com.tcb.springboot;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "predictlte")
public class Predictbean implements Serializable {
	public Predictbean() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Predictbean(Integer id, Double pdsii,Integer day,Integer hours) {
		super();
		this.id = id;
		this.pdsii = pdsii;
		this.day=day;
		this.hours=hours;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Double getPdsii() {
		return pdsii*1000;
	}


	public void setPdsii(Double pdsii) {
		this.pdsii = pdsii;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	private static final long serialVersionUID = 1L;

	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Integer id;
	    private Integer day;
	    private Integer hours;
	    public Integer getDay() {
			return day;
		}


		public void setDay(Integer day) {
			this.day = day;
		}


		public Integer getHours() {
			return hours;
		}


		public void setHours(Integer hours) {
			this.hours = hours;
		}


		private Double pdsii ;

	    
}

