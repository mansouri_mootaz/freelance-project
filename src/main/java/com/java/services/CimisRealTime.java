package com.java.services;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.java.dao.MysqlConnection;
import com.java.dao.ParserLine;
import com.java.dao.SQLRequests;
import com.tcb.predictlayer.SingeltonSparkContext;

public class CimisRealTime {
	private static final String TABLENAME = "predictlte";

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
		CimisRealTime.getRealTimeData("prediction");
	}

	public static void getRealTimeData(String topicname) throws IOException, ClassNotFoundException, SQLException {

		SparkContext sc = SingeltonSparkContext.getSparkConetxt();
		JavaSparkContext jssc = new JavaSparkContext(sc);
		LinearRegressionModel sameModel = LinearRegressionModel.load(jssc.sc(), "javaLinearRegressionWithSGDModel");
		Integer counter = new Integer(0);
		boolean tablecreated = SQLRequests.createTablePredictedValue(MysqlConnection.getConnection(), TABLENAME);
		Response response = Jsoup.connect("http://www.cimis.water.ca.gov/WSNReportCriteria.aspx") //
				.method(Method.GET).execute();
		Document getDocument = response.parse();
		Map<String, String> cookies = response.cookies();
		String mainContentRadScriptManager1TSM = "";
		for (Element script : getDocument.getElementsByTag("script")) {
			if (script.attr("src").startsWith("/Telerik.Web.UI.WebResource.axd?"))
				mainContentRadScriptManager1TSM = URLDecoder.decode(script.attr("src").substring(117), "UTF-8");
		}
		mainContentRadScriptManager1TSM = mainContentRadScriptManager1TSM.replace("\n", "").replace("\r", "");
		Map<String, String> data = new HashMap<String, String>();
		data.put("MainContent_RadScriptManager1_TSM", mainContentRadScriptManager1TSM);
		data.put("__EVENTTARGET", getDocument.getElementById("__EVENTTARGET").attr("value"));
		data.put("__EVENTARGUMENT", getDocument.getElementById("__EVENTARGUMENT").attr("value"));
		data.put("__VIEWSTATE", getDocument.getElementById("__VIEWSTATE").attr("value"));
		data.put("__VIEWSTATEGENERATOR", getDocument.getElementById("__VIEWSTATEGENERATOR").attr("value"));
		data.put("__PREVIOUSPAGE", getDocument.getElementById("__PREVIOUSPAGE").attr("value"));
		data.put("ctl00$MainContent$RadScriptManager1",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$RadScriptManager1").attr("value"));
		data.put("ctl00$MainContent$reportType",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$reportType").attr("value"));
		data.put("ctl00$MainContent$limitedReportType",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$limitedReportType").attr("value"));
		data.put("ctl00$MainContent$output",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$output").attr("value"));
		data.put("ctl00$MainContent$unit",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$unit").attr("value"));
		data.put("ctl00$MainContent$selectedStations", "140");
		data.put("ctl00$MainContent$selectedHourlySensors", getDocument
				.getElementsByAttributeValue("name", "ctl00$MainContent$selectedHourlySensors").attr("value"));
		data.put("ctl00$MainContent$selectedDailySensors", getDocument
				.getElementsByAttributeValue("name", "ctl00$MainContent$selectedDailySensors").attr("value"));
		data.put("ctl00$MainContent$inactiveStationNumbers", getDocument
				.getElementsByAttributeValue("name", "ctl00$MainContent$inactiveStationNumbers").attr("value"));
		data.put("ctl00$MainContent$nonEtoStationNumbers", getDocument
				.getElementsByAttributeValue("name", "ctl00$MainContent$nonEtoStationNumbers").attr("value"));
		data.put("ctl00$MainContent$wasUserAuthenticated", getDocument
				.getElementsByAttributeValue("name", "ctl00$MainContent$wasUserAuthenticated").attr("value"));
		data.put("ctl00$MainContent$isError",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$isError").attr("value"));
		data.put("ctl00$MainContent$cbSampleReportType",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$cbSampleReportType").attr("value"));
		data.put("ctl00_MainContent_cbSampleReportType_ClientState",
				"{\"logEntries\":[],\"value\":\"HOURLY\",\"text\":\"Limited Hourly Report\",\"enabled\":true}");
		data.put("ctl00$MainContent$cbSampleUnitOfMeasure", getDocument
				.getElementsByAttributeValue("name", "ctl00$MainContent$cbSampleUnitOfMeasure").attr("value"));
		data.put("ctl00_MainContent_cbSampleUnitOfMeasure_ClientState",
				"{\"logEntries\":[],\"value\":\"E\",\"text\":\"English Units\",\"enabled\":true}");
		data.put("ctl00$MainContent$grdStationsReport$txtTotalStations",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$grdStationsReport$txtTotalStations")
						.attr("value"));
		data.put("ctl00$MainContent$txtTotalStations",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$txtTotalStations").attr("value"));
		data.put("ctl00_MainContent_grdHourlySensors_ClientState", getDocument
				.getElementsByAttributeValue("name", "ctl00_MainContent_grdHourlySensors_ClientState").attr("value"));
		data.put("ctl00_MainContent_grdDailySensors_ClientState", getDocument
				.getElementsByAttributeValue("name", "ctl00_MainContent_grdDailySensors_ClientState").attr("value"));
		data.put("ctl00$MainContent$btnSubmit",
				getDocument.getElementsByAttributeValue("name", "ctl00$MainContent$btnSubmit").attr("value"));

		for (int i = 0; i < 153; i++) {
			data.put("ctl00_MainContent_grdStationsReport_grdStations_ClientState", "{\"selectedIndexes\":[\"" + i
					+ "\"],\"reorderedColumns\":[],\"expandedItems\":[],\"expandedGroupItems\":[],\"expandedFilterItems\":[],\"deletedItems\":[],\"hidedColumns\":[],\"showedColumns\":[],\"scrolledPosition\":\"44,0\",\"popUpLocations\":{},\"draggedItemsIndexes\":[]}");

			Document doc = Jsoup.connect("http://www.cimis.water.ca.gov/WSNReportCriteria.aspx") //
					.userAgent(
							"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36") //
					.header("Content-Type", "application/x-www-form-urlencoded") //
					.header("Origin", "http://www.cimis.water.ca.gov") //
					.header("Referer", "http://www.cimis.water.ca.gov/WSNReportCriteria.aspx") //
					.header("Upgrade-Insecure-Requests", "1") //
					.cookies(cookies) //
					.data(data) //
					.post();

			Elements outsideTables = doc.select("table.outside_table");
			Element targetTable = outsideTables.get(outsideTables.size() - 1);
			Element lastActiveElement = null;

			Elements trs = targetTable.getElementsByTag("tbody").first().children();

			for (Element tr : trs) {
				if (!tr.text().contains("A") && !tr.text().contains("B") && !tr.text().contains("C")
						&& !tr.text().contains("D") && !tr.text().contains("E") && !tr.text().contains("F")
						&& !tr.text().contains("G") && !tr.text().contains("H") && !tr.text().contains("I")
						&& !tr.text().contains("G") && !tr.text().contains("K") && !tr.text().contains("L")
						&& !tr.text().contains("M") && !tr.text().contains("N") && !tr.text().contains("O")
						&& !tr.text().contains("P") && !tr.text().contains("Q") && !tr.text().contains("R")
						&& !tr.text().contains("S") && !tr.text().contains("T") && !tr.text().contains("U")
						&& !tr.text().contains("V") && !tr.text().contains("W") && !tr.text().contains("X")
						&& !tr.text().contains("Y") && !tr.text().contains("Z") && !tr.text().contains("Tots/Avgs"))
					lastActiveElement = tr;
			}
			if (lastActiveElement != null) {
				double[] features = new double[lastActiveElement.text().split(" ").length];
				String[] array = lastActiveElement.text().split(" ");
				features[0] = ParserLine.parseEtoRealTime(array);
				features[1] = ParserLine.parsePerciRealTime(array);
				features[2] = ParserLine.parseSolaRealTime(array);
				features[3] = ParserLine.parseAirRealTime(array);
				features[4] = ParserLine.parserealtHimRealTime(array);
				features[5] = ParserLine.parsedewRealTime(array);
				features[7] = ParserLine.parsewindRealTime(array);
				features[8] = ParserLine.parsewin_dirRealTime(array);
				features[9] = ParserLine.parseSoilRealTime(array);

				try {
					System.out.println(sameModel.predict(Vectors.dense(features)));
					Double result = sameModel.predict(Vectors.dense(features));

					if (tablecreated) {
						Date date = new Date();
						Integer currentDay = new Integer(date.getDay());
						Integer currentHour = new Integer(date.getHours());
						Integer currentYear = new Integer(date.getYear()) + 1900;
						System.out.println(result + "");
						System.out.println("table created");
						SQLRequests.insertIntoTablePredictedValue(MysqlConnection.getConnection(), TABLENAME, counter,
								result, currentYear, currentDay, currentHour);

						counter++;
					}
				} catch (Exception e) {
				}
			}
			jssc.close();
		}
	}

}
