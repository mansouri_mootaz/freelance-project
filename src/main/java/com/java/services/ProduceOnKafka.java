package com.java.services;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

/***
 * produce data on KAFKA:
 * there is two flow of data CIMIS and PDSI
 * this program Put this tow Flow on tow topic "PDSI" and CIMIS"
 * */
public class ProduceOnKafka {
	private static KafkaProducer<String, String> PRODUCER = null;
	/**
	 * @param input_file
	 * @throws IOException
	 */
	public static void putDataFlow1(String input_file,String topicname)  throws IOException {
  	         InputStream input = new FileInputStream(input_file);
	         GZIPInputStream gzis = new GZIPInputStream(input);
             InputStreamReader xover = new InputStreamReader(gzis);
             BufferedReader is = new BufferedReader(xover);
             Integer  counter = new Integer(1);
             String line = null ;
            while (((line = is.readLine()) != null)){
            	if ((counter >=33)){
            String [] array = CleaningCimisData.splitString(line);
            int i = 0;
            if((array.length > 3)){
            	if ( array[2].equals(" 36.25" )&&array[3].equals(" -118.7500")){
	    	          i = CleaningCimisData.getDate(line);
	    	          if (i > 1996){
	    	        	  System.out.println(line);
	    	              PRODUCER.send(new ProducerRecord<String, String>(topicname, counter.toString(), line));
	    	          }
            	}            	
            }
        	counter ++;
            	} else counter ++;        	
                     }
            is.close();
	    	xover.close();	
	}
	/**
	 * @param input_file
	 * @param topicname
	 * @throws IOException
	 */
	public static void putDataFlow2(String input_file,String topicname)  throws IOException {
	    	 InputStream input = new FileInputStream(input_file);
	         ZipInputStream zip = new ZipInputStream(input);
	         BufferedReader is = null;
	         InputStreamReader xover = new InputStreamReader(zip);
	         Integer counter = new Integer(1);
	            while ((zip.getNextEntry())!=null){
	                 is = new BufferedReader(xover);
	                String line = null ;
	                while (((line = is.readLine())!=null)) {
	    	        	System.out.println(line);
	                	PRODUCER.send(new ProducerRecord<String, String>(topicname, counter.toString(), line));
	                	counter++;
	    			         }
         	                }
	            is.close();
                xover.close();
     }
	public static void PutRealTimeDataOnKafka(String topicname,String line){
		if (line != null){
        	PRODUCER.send(new ProducerRecord<String, String>(topicname, line, line));
        	System.out.println(line);
		}
	}
	/**
	 * @param brokers
	 * @return
	 */
	public static KafkaProducer<String, String> initProducer(String brokers){  		  		 
		    Properties props = new Properties();
		    props.put("bootstrap.servers", brokers);
		    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		     PRODUCER = new KafkaProducer<String, String>(props);
		    System.out.println("Producer was Created");
			return PRODUCER;
                    }
	}
