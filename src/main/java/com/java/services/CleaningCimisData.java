package com.java.services;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Logger;

import com.java.dao.MysqlConnection;
import com.java.dao.ParserLine;
import com.java.dao.SQLRequests;

public class CleaningCimisData {
	private static KafkaConsumer<String, String> kafkaConsumer = null;
	static ArrayList<String> list = new ArrayList<String>();
	static Logger log = Logger.getLogger(CleaningCimisData.class);

	/**
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void cleaningFlow1(String tablename, String topicname) throws ClassNotFoundException, SQLException {
		boolean tablecreate = false;
		boolean tableinsert = false;
		ConsumerRecords<String, String> records = null;
		Integer counter = new Integer(1);
		CleaningCimisData.initConsumer(topicname);
		tablecreate = SQLRequests.createTableFlow1(MysqlConnection.getConnection(), tablename);
		System.out.println("table flow 1 created");
		while ((records = kafkaConsumer.poll(1000)) != null) {
			for (ConsumerRecord<String, String> record : records) {
				if (tablecreate)
					log.info("table created");
				String[] array = CleaningCimisData.splitString(record.value());
				tableinsert = SQLRequests.insertIntoTableFlow1(MysqlConnection.getConnection(), tablename, counter,
						ParserLine.parserDateflow1(array), ParserLine.parserLatflow1(array),
						ParserLine.parserLonflow1(array), ParserLine.parserPdsiflow1(array));
				System.out.println(record.value());
				if (tableinsert)
					System.out.println(" unable to create table ");
				else
					System.out.println("Table was Created ");
				counter++;
			}
		}
	}

	/**
	 * 
	 * @param str
	 * @return array of string
	 */
	public static String[] splitString(String str) {
		String array[] = str.split(",");
		return array;
	}

	/**
	 * 
	 * @param date
	 * @return date
	 */
	public static int getDate(String date) {
		int j = 0;
		if (date != null) {
			if (!date.isEmpty()) {
				String[] array1 = date.split(",");
				if (array1.length > 3) {
					String[] array2 = array1[1].split("-");
					j = Integer.parseInt(array2[0].replaceAll("'", "").trim());
				} else
					System.out.println("String not confirmed");
			} else
				System.out.println("String is empty");
		} else
			System.out.println("String Null");
		return j;
	}

	/**
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void cleaningFlow2(String tablename, String topicname) throws ClassNotFoundException, SQLException {

		CleaningCimisData.initConsumer(topicname);
		boolean tablecreate = false;
		boolean tableinsert = false;
		Integer counter = new Integer(1);
		tablecreate = SQLRequests.createTableFlow2(MysqlConnection.getConnection(), tablename);
		if (tablecreate)
			log.info("table created");
		else
			log.error("unable to create table");
		ConsumerRecords<String, String> records = kafkaConsumer.poll(1000);
		while (records != null) {
			for (ConsumerRecord<String, String> record : records) {
				if (!record.value().contains("Stn Id,Stn Name,CIMIS")) {
					String[] array = splitString(record.value());
					if (array.length > 22) {
						System.out.println(record.value());
						tableinsert = SQLRequests.insertIntoTableFlow2(MysqlConnection.getConnection(), tablename,
								counter,ParserLine.parserRegionFlow2(array), ParserLine.parserDateFlow2(array), ParserLine.parserHourFlow2(array),
								ParserLine.parserEtoFlow2(array), ParserLine.parserPercipiFlow2(array),
								ParserLine.parserSolarRadiationFlow2(array), ParserLine.parserVaporPressureFlow2(array),
								ParserLine.parseraArTemperatureFlow2(array),
								ParserLine.parserRelativeHumidityFlow2(array), ParserLine.parserDewPointFlow2(array),
								ParserLine.parserWindSpeedFlow2(array), ParserLine.parserWindDirectioneFlow2(array),
								ParserLine.parserSoilTemperatureFlow2(array));
						if (tableinsert)
							log.info(record.value());
						else
							log.error("Exception when inserting data on table");
						counter++;
					}
				}
			}
			records = kafkaConsumer.poll(1000);
		}

	}

	/**
	 * 
	 * @param array
	 * @return date of flow1
	 */
	public static Date formatDateflow1(String[] array) {
		java.util.Date date = new java.util.Date();
		if (array.length > 3) {
			String dateInString = array[1].replaceAll("'", "").trim();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				date = formatter.parse(dateInString);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	public static void consumeDate() {

	}

	/**
	 * Initiate the consumer KAFKA
	 */
	public static void initConsumer(String topic) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "mootaz-Latitude-E5440:6667");
		props.put("group.id", "group-17");
		props.put("enable.auto.commit", "true");
		props.put("auto.commit.interval.ms", "1000");
		props.put("auto.offset.reset", "earliest");
		props.put("session.timeout.ms", "30000");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		kafkaConsumer = new KafkaConsumer<String, String>(props);
		kafkaConsumer.subscribe(Arrays.asList(topic));
	}
}
