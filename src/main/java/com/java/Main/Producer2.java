package com.java.Main;

import java.io.IOException;

import com.java.services.ProduceOnKafka;
/**
 * Produce data on KAFKA
 */
public class Producer2 {

	private final  static   String   INPUT_FILE_2 = "/home/mootaz/freelance-files/hourly.zip";
	private final  static   String   TOPIC_FLOW_2 ="topicflow22";
	private final  static   String   BROKERS      = "mootaz-Latitude-E5440:6667";

	public static void main(String[] args) throws IOException {
		ProduceOnKafka.initProducer(BROKERS);
		ProduceOnKafka.putDataFlow2(INPUT_FILE_2,TOPIC_FLOW_2);
	}

}
