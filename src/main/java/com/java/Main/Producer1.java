package com.java.Main;

import java.io.IOException;

import com.java.services.ProduceOnKafka;

/**
 * Produce data on KAFKA
 */
public class Producer1 {

	private final static String INPUT_FILE_1 = "/home/mootaz/freelance-files/dump.sql.gz";
	private final static String TOPIC_FLOW_1 = "topicflow16";
	private final static String BROKERS = "mootaz-Latitude-E5440:6667";

	public static void main(String[] args) throws IOException {
		ProduceOnKafka.initProducer(BROKERS);
		ProduceOnKafka.putDataFlow1(INPUT_FILE_1, TOPIC_FLOW_1);
	}

}
