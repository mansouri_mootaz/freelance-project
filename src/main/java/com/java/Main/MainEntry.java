package com.java.Main;

import java.io.IOException;
import java.sql.SQLException;

import com.java.services.CimisRealTime;
import com.java.services.CleaningCimisData;
import com.java.services.ProduceOnKafka;

public class MainEntry  {

	private final static String INPUT_FILE_1 = "/home/mootaz/freelance-files/dump.sql.gz";
	private final static String TOPIC_FLOW_1 = "topicflow16";
	private final static String INPUT_FILE_2 = "/home/mootaz/freelance-files/hourly.zip";
	private final static String TOPIC_FLOW_2 = "topicflow22";
	private static final String  TABLE_FLOW_1 ="montest34";
	private static final String  TABLE_FLOW_2  = "montest28";
	private final static String BROKERS = "mootaz-Latitude-E5440:6667";

	public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException {

		// Producer 1
		ProduceOnKafka.initProducer(BROKERS);
		ProduceOnKafka.putDataFlow1(INPUT_FILE_1, TOPIC_FLOW_1);

		// Producer 2
		ProduceOnKafka.putDataFlow1(INPUT_FILE_1, TOPIC_FLOW_1);
		
		//Cleaning the first flow
		try {
			CleaningCimisData.cleaningFlow1(TABLE_FLOW_1, TOPIC_FLOW_1);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			 e.getMessage();		}
		
		//Cleaning the second flow 
		try{
			CleaningCimisData.cleaningFlow2(TABLE_FLOW_2, TOPIC_FLOW_2);
		}catch(ClassNotFoundException e){
			e.getMessage();
		} catch (SQLException e) {
            e.getMessage();
		}

		// real time data
		CimisRealTime.getRealTimeData("prediction");
	}

}
