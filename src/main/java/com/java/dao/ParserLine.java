package com.java.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParserLine {
	/**
	 * @param array
	 * @return latitude
	 */
	public static Double parserLatflow1(String[] array) {
		String lat = array[2];
		return new Double(lat);
	}

	/**
	 * @param array
	 * @return Longitude
	 */
	public static Double parserLonflow1(String[] array) {
		String lat = array[3];
		return new Double(lat);
	}

	/**
	 * @param array
	 * @return PDSI
	 */
	public static Double parserPdsiflow1(String[] array) {
		String lat = array[4];
		return new Double(lat.replaceAll("\\)", ""));
	}

	/**
	 * @param array
	 * @return date
	 */
	public static Date parserDateflow1(String[] array) {
		java.util.Date date = new java.util.Date();
		if (array.length > 3) {
			String dateInString = array[1].replaceAll("'", "").trim();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			try {
				date = formatter.parse(dateInString);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	/**
	 * @param array
	 * @return regular date
	 */
	public static Date parserDateFlow2(String[] array) {
		java.util.Date date = new java.util.Date();
		System.out.println(array[3]);
		SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");
		try {
			date = formatter.parse(array[3]);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * @param array
	 * @return specific region
	 */
	public static String parserRegionFlow2(String[] array) {
		
		String region ="";
		
		System.out.println(array[2]);
		if (!array[2].isEmpty()) {
			region = array[2];
		}
		return region;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserHourFlow2(String[] array) {
		Double hour = new Double(00);
		if (!array[4].isEmpty()) {
			hour = new Double(array[4]);
		}
		return hour;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserEtoFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[5].isEmpty()) {
			eto = new Double(array[5]);
		}
		return eto;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserPercipiFlow2(String[] array) {
		Double percip = new Double(0);
		if (!array[6].isEmpty()) {
			percip = new Double(array[6]);
		}
		return percip;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserSolarRadiationFlow2(String[] array) {
		Double solar_radiation = new Double(0);
		if (!array[8].isEmpty()) {
			solar_radiation = new Double(array[8]);
		}
		return solar_radiation;
	}

	/**
	 * 
	 * @param array
	 * @return
	 */
	public static Double parserVaporPressureFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[10].isEmpty()) {
			eto = new Double(array[10]);
		}
		return eto;
	}

	/**
	 * 
	 * @param array
	 * @return
	 */
	public static Double parseraArTemperatureFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[12].isEmpty()) {
			eto = new Double(array[12]);
		}
		return eto;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserRelativeHumidityFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[14].isEmpty()) {
			eto = new Double(array[14]);
		}
		return eto;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserDewPointFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[16].isEmpty()) {
			eto = new Double(array[16]);
		}
		return eto;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserWindSpeedFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[18].isEmpty()) {
			eto = new Double(array[18]);
		}
		return eto;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserWindDirectioneFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[20].isEmpty()) {
			eto = new Double(array[20]);
		}
		return eto;
	}

	/**
	 * @param array
	 * @return
	 */
	public static Double parserSoilTemperatureFlow2(String[] array) {
		Double eto = new Double(0);
		if (!array[22].isEmpty()) {
			eto = new Double(array[22]);
		}
		return eto;
	}

	public static Double parseEtoRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[1].isEmpty()) && (ParserLine.isNumber(array[1]))) {
			eto = new Double(array[1]);
		}
		return eto;
	}

	public static Double parsePerciRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[2].isEmpty())) {
			eto = new Double(array[2]);
		}
		return eto;
	}

	public static Double parseSolaRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[3].isEmpty())) {
			eto = new Double(array[3]);
		}
		return eto;
	}

	public static Double parsevaporRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[4].isEmpty())) {
			eto = new Double(array[4]);
		}
		return eto;
	}

	public static Double parseAirRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[5].isEmpty())) {
			eto = new Double(array[5]);
		}
		return eto;
	}

	public static Double parserealtHimRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[6].isEmpty())) {
			eto = new Double(array[6]);
		}
		return eto;
	}

	public static Double parsedewRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[7].isEmpty())) {
			eto = new Double(array[7]);
		}
		return eto;
	}

	public static Double parsewindRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[8].isEmpty())) {
			eto = new Double(array[8]);
		}
		return eto;
	}

	public static Double parsewin_dirRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[9].isEmpty())) {
			eto = new Double(array[9]);
		}
		return eto;
	}

	public static Double parseSoilRealTime(String[] array) {
		Double eto = new Double(0);
		if ((!array[10].isEmpty())) {
			eto = new Double(array[10]);
		}
		return eto;
	}

	public static boolean isNumber(String str) {
		boolean number = false;
		if ((str.charAt(0) == '0') || (str.charAt(0) == '1') || (str.charAt(0) == '2') || (str.charAt(0) == '3')
				|| (str.charAt(0) == '4') || (str.charAt(0) == '5') || (str.charAt(0) == '6') || (str.charAt(0) == '7')
				|| (str.charAt(0) == '8') || (str.charAt(0) == '9')) {
			number = true;
		}
		return number;
	}

}
