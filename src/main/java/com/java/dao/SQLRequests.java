package com.java.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

public class SQLRequests {
	/**
	 * @param connection
	 * @param tablename
	 * @return true if table is created
	 * @throws SQLException
	 */
	public static boolean createTablePredictedValue(Connection connection, String tablename) throws SQLException {
		Statement statement = null;
		String sql = "CREATE TABLE IF NOT EXISTS " + tablename + "(" + "id   Integer(5) NOT NULL  , "
				+ "pdsii Double NOT NULL ," + "year Integer(5) NOT NULL ," + "day Integer(5) NOT NULL ,"
				+ "hours Integer(5) NOT NULL, " + " PRIMARY KEY (id) " + ")";
		statement = connection.createStatement();
		boolean resultSet = statement.execute(sql);
		return !resultSet;
	}

	public static boolean insertIntoTablePredictedValue(Connection connection, String tablename, Integer id,
			Double pdsi, Integer year, Integer day, Integer hours) throws SQLException {
		boolean resultSet = false;
		PreparedStatement ps = connection
				.prepareStatement("insert into " + tablename + " (id,pdsii,year,day,hours) values(?,?,?,?,?)");
		ps.setInt(1, id);
		ps.setDouble(2, pdsi);
		ps.setInt(3, year);
		ps.setInt(4, day);
		ps.setInt(5, hours);

		resultSet = ps.execute();
		return !resultSet;
	}

	// public static boolean insertIntoTablePredictedValue(Connection
	// connection,String tablename,Integer id,Double pdsi) throws SQLException {
	// boolean resultSet = false ;
	// PreparedStatement ps=connection.prepareStatement("insert into " +
	// tablename +" (id,pdsii) values(?,?)");
	// ps.setInt(1, id);
	// ps.setDouble(2, pdsi);
	// resultSet = ps.execute();
	// return !resultSet;
	// }
	public static boolean createTableFlow1(Connection connection, String tablename) throws SQLException {
		Statement statement = null;
		String sql = "CREATE TABLE IF NOT EXISTS " + tablename + "(" + "id   Integer(5) NOT NULL , "
				+ "date  Date NOT NULL , " + "lat   Double  NOT NULL ," + "lon   Double NOT NULL,"
				+ "pdsii Double NOT NULL ," + "PRIMARY KEY (id) " + ")";
		statement = connection.createStatement();
		boolean resultSet = statement.execute(sql);
		return !resultSet;
	}

	/**
	 * @param connection
	 * @param tablename
	 * @param id
	 * @param date
	 * @param lat
	 * @param lon
	 * @param pdsi
	 * @return true if data is inserted
	 * @throws SQLException
	 */
	public static boolean insertIntoTableFlow1(Connection connection, String tablename, Integer id, Date date,
			Double lat, Double lon, Double pdsi) throws SQLException {
		boolean resultSet = false;
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		PreparedStatement ps = connection
				.prepareStatement("insert into " + tablename + " (id,date,lat,lon,pdsii) values(?,?,?,?,?)");
		ps.setInt(1, id);
		ps.setDate(2, sqlDate);
		ps.setDouble(3, lat);
		ps.setDouble(4, lon);
		ps.setDouble(5, pdsi);
		resultSet = ps.execute();
		return !resultSet;
	}

	/***
	 * @param connection
	 * @param tablename
	 * @param id
	 * @param hours
	 * @param Julian_Date
	 * @param Reference_eTo
	 * @param precipitation
	 * @param solar_radiation
	 * @param vapor_pressure
	 * @param air_temperature
	 * @param relative_humidity
	 * @param dew_point
	 * @param wind_speed
	 * @param wind_direction
	 * @param soil_temperature
	 * @return true if data is inserted
	 * @throws SQLException
	 */
	public static boolean insertIntoTableFlow2(Connection connection, String tablename, Integer id,String region, Date Julian_Date,
			Double hour, Double Reference_eTo, Double precipitation, Double solar_radiation, Double vapor_pressure,
			Double air_temperature, Double relative_humidity, Double dew_point, Double wind_speed,
			Double wind_direction, Double soil_temperature) throws SQLException {
		boolean resultSet = false;
		java.sql.Date sqlDate = new java.sql.Date(Julian_Date.getTime());
		PreparedStatement ps = connection.prepareStatement("insert into " + tablename + "(id," +"region,"+ "Julian_Date,"
				+ "hours, " + "Reference_eTo," + "precipitation" + ",solar_radiation," + "vapor_pressure,"
				+ "air_temperature," + "relative_humidity," + "dew_point" + ",wind_speed," + "wind_direction,"
				+ "soil_temperature) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		ps.setInt(1, id);
		ps.setString(2,region);
		ps.setDate(3, sqlDate);
		ps.setDouble(4, hour);
		ps.setDouble(5, Reference_eTo);
		ps.setDouble(6, precipitation);
		ps.setDouble(7, solar_radiation);
		ps.setDouble(8, vapor_pressure);
		ps.setDouble(9, air_temperature);
		ps.setDouble(10, relative_humidity);
		ps.setDouble(11, dew_point);
		ps.setDouble(12, wind_speed);
		ps.setDouble(13, wind_direction);
		ps.setDouble(14, soil_temperature);
		System.out.println(ps);
		resultSet = ps.execute();

		return !resultSet;
	}

	/**
	 * 
	 * @param connection
	 * @param tablename
	 * @return true iff table is created
	 * @throws SQLException
	 */
	public static boolean createTableFlow2(Connection connection, String tablename) throws SQLException {
		Statement statement = null;
		String sql = "CREATE TABLE IF NOT EXISTS " + tablename + "(" + " id  Integer(5) NOT NULL, "
				+ "region String NOT NULL," + "Julian_Date     Date NOT NULL," + "hours           Double   , "
				+ "Reference_eTo   Double    , " + "precipitation   Double    ," + "solar_radiation Double   ,"
				+ "vapor_pressure  Double  ," + "air_temperature Double  ," + "relative_humidity Double ,"
				+ "dew_point         Double         ," + "wind_speed        Double      ," + "wind_direction Double,"
				+ "soil_temperature Double," + "PRIMARY KEY (id) " + ")";
		statement = connection.createStatement();
		boolean resultSet = statement.execute(sql);
		return !resultSet;
	}

	/**
	 * @param connection
	 * @param tablename
	 * @return boolean
	 * @throws SQLException
	 */
	public static boolean createTablefeatures(Connection connection, String tablename) throws SQLException {
		Statement statement = null;
		String sql = "CREATE TABLE IF NOT EXISTS " + tablename + "(" + " id        Integer(5) NOT NULL AUTO_INCREMENT, "
				+ "region CHAR(20) NOT NULL," + "Julian_Date     Date NOT NULL," + "hours           Double    , "
				+ "Reference_eTo   Double    , " + "precipitation   Double    ," + "solar_radiation Double    ,"
				+ "vapor_pressure  Double    ," + "air_temperature Double    ," + "relative_humidity Double  ,"
				+ "dew_point         Double  ," + "wind_speed        Double  ," + "wind_direction Double     ,"
				+ "soil_temperature Double   ," + "pdsii Double NOT NULL     , " + "PRIMARY KEY (id) " + ")";
		statement = connection.createStatement();
		boolean resultSet = statement.execute(sql);
		return !resultSet;
	}

	public static boolean insertIntoTableFeatures(Connection connection, String tablename, String table_flow1,
			String table_flow2) throws SQLException {
		boolean resultSet = false;
		PreparedStatement ps = connection.prepareStatement("insert into " + tablename + "(id," + "region,"
				+ "Julian_Date," + "hours, " + "Reference_eTo," + "precipitation" + ",solar_radiation,"
				+ "vapor_pressure," + "air_temperature," + "relative_humidity," + "dew_point" + ",wind_speed,"
				+ "wind_direction," + "soil_temperature , pdsii) " + "SELECT NULL,region, Julian_Date, "
				+ "hours,Reference_eTo,precipitation," + "solar_radiation,vapor_pressure,"
				+ "air_temperature,relative_humidity," + "dew_point,wind_speed,wind_direction," + "soil_temperature,"
				+ "pdsii " + "FROM " + table_flow1 + " ," + table_flow2 + " WHERE " + table_flow1 + ".date ="
				+ table_flow2 + ".Julian_Date");

		System.out.println(ps);
		resultSet = ps.execute();

		return !resultSet;
	}

	public static boolean createTablePrediction(Connection connection, String tablename) throws SQLException {
		Statement statement = null;
		String sql = "CREATE TABLE IF NOT EXISTS " + tablename + "(" + " id        Integer(5) NOT NULL AUTO_INCREMENT, "
				+ "Julian_Date     Date NOT NULL," + "hours           Double    , " + "Reference_eTo   Double    , "
				+ "precipitation   Double    ," + "solar_radiation Double    ," + "vapor_pressure  Double    ,"
				+ "air_temperature Double    ," + "relative_humidity Double  ," + "dew_point         Double  ,"
				+ "wind_speed        Double  ," + "wind_direction Double     ," + "soil_temperature Double   ,"
				+ "pdsii Double NOT NULL     , " + "prediction Double NOT NULL ," + "PRIMARY KEY (id) " + ")";
		statement = connection.createStatement();
		boolean resultSet = statement.execute(sql);
		return !resultSet;
	}
}
