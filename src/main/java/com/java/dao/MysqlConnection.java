package com.java.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class MysqlConnection {
	
	private static   Connection connection = null ;
	private MysqlConnection() throws ClassNotFoundException, SQLException{	
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/weather","root", "Azerty1");
//		connection = DriverManager.getConnection("jdbc:mysql://localhost/?user=root&password=");
	     }
	@SuppressWarnings("static-access")
	public  static Connection getConnection() throws ClassNotFoundException, SQLException{				
		return connection == null ? new MysqlConnection().connection : connection ;
	       }

}
